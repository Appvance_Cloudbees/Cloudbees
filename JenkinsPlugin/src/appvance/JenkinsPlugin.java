/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package appvance;

/**
 *
 * @author Luis
 */
public class JenkinsPlugin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        AppvanceRestClient arc = new AppvanceRestClient(args[0]);
        arc.logIn("appvance","appvance");
        arc.startScenario(args[1]);
        Thread.sleep(10000);
        System.out.println(arc.isRunning());
        while (arc.isRunning()){
            System.out.print(arc.getNextOutput());
            Thread.sleep(1000);
        }
        System.out.println(arc.getReportURL());
        int success = arc.getSuccess();
        int failures = arc.getFailures();
        if (success == 0){
            throw new Exception("No successful transactions.");
        }
        if (failures > success){
            throw new Exception("More failures than success transactions.");
        }
        if (failures > 0){
            System.err.println(failures+" failures trasactions during the test.");
        }
    }
}
